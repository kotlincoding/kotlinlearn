fun main(args: Array<String>)
{
    val age: Long = 35L
    val a: Boolean = false
    val b: Char = 'c'
    val c: Double = 77.1
    val d: Float = 77.1F

    /* Strings */

    val s: String = "Misterclass"
    val s1: String = "Mister\nClass"
    val multiS: String = """ 
        Mister 
        Clas
        Ttttt"""

    /* String templates */

    val firstName = "mister"
    val lastName = "class"
    val name = "My name is $firstName $lastName"
    println(name)

    /* Any - basic of all types */
    val r:Any = 10
    val r1:Any = "Mister"

    /* Dynamic type */
    val ddd = 5
    val ccc = "mister" //YOU MUST INITIALIZE YOUR VARIABLE
}